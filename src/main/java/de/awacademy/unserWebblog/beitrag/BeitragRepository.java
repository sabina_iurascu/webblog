package de.awacademy.unserWebblog.beitrag;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BeitragRepository extends JpaRepository<Beitrag, Long> {
    List<Beitrag> findAllByOrderByErstellungsDatumDesc();
}
